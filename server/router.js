var express=require('express');
var router=express.Router();
var controller=require('./controller');
var mainController=require('../../../main/server/controller');


//Author
router.get('/instances', controller.checkAuth, controller.getInstances);
router.delete('/instances/:id', controller.checkAuth, controller.delInstance);
router.post('/instances', controller.checkAuth, controller.createInstance);
router.get('/results/:id', controller.checkAuth, controller.getResults);

router.get('/surveys', controller.checkAuth, controller.getSurveys);
router.post('/surveys', controller.checkAuth, controller.saveSurvey);
router.put('/surveys/:id', controller.checkAuth, controller.saveSurvey);
router.delete('/surveys/:id', controller.checkAuth, controller.deleteSurvey);

//Participants
router.get('/part/instances/:ticket',controller.loadInstanceFromTicket, controller.getPartInstance)
router.post('/part/votes',controller.loadInstanceFromTicket, controller.addVote)

//Admin
router.get('/admin/instances', mainController.checkAdmin, controller.getAdminInstances);
router.get('/admin/surveys', mainController.checkAdmin, controller.getAdminSurveys);

module.exports=router;
