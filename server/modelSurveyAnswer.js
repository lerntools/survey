var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var SurveyAnswerSchema=new Schema({
	instance:	{type: String},
	answers:	[  {type: String, max: 300}]
});

module.exports=mongoose.model('SurveyAnswer', SurveyAnswerSchema );
