<a href="https://translate.codeberg.org/engage/lerntools/">
<img src="https://translate.codeberg.org/widgets/lerntools/-/survey/svg-badge.svg" alt="Übersetzungsstatus" />
</a>

# survey

Create template based polls.

This repository is an optional module for the "lerntools". For installation and configuration, please see the documentation in https://codeberg.org/lerntools/base
